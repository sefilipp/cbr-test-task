package ru.cbr.atlas.jira.testtask.field;

import com.atlassian.jira.issue.customfields.CustomFieldType;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.config.FieldConfig;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.velocity.VelocityManager;
import org.junit.Test;
import ru.cbr.atlas.jira.testtask.manager.LinkCustomFieldConfigManager;

import static org.junit.Assert.*;
import static org.mockito.Matchers.anyMap;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class LinkCustomFieldConfigTest {
    @Test
    public void testGetDisplayName_validName() throws Exception {
        LinkCustomFieldConfig config = new LinkCustomFieldConfig(null, null, null);
        String result = config.getDisplayName();
        assertEquals("ru.cbr.atlas.jira.testtask.field.config.name", result);
    }

    @Test
    public void testGetDisplayNameKey_validNameKey() throws Exception {
        LinkCustomFieldConfig config = new LinkCustomFieldConfig(null, null, null);
        String result = config.getDisplayNameKey();
        assertEquals("ru.cbr.atlas.jira.testtask.field.config.name", result);
    }

    @Test
    public void testGetViewHtml_validParams_validHtml() throws Exception {
        FieldConfig fieldConfig = mock(FieldConfig.class);
        JiraAuthenticationContext jiraAuthenticationContext = mock(JiraAuthenticationContext.class);
        VelocityManager velocityManager = mock(VelocityManager.class);
        LinkCustomFieldConfigManager linkCustomFieldConfigManager = mock(LinkCustomFieldConfigManager.class);

        when(velocityManager.getBody(anyString(), anyString(), anyMap())).thenReturn("html");

        LinkCustomFieldConfig config = new LinkCustomFieldConfig(jiraAuthenticationContext, velocityManager, linkCustomFieldConfigManager);
        String result = config.getViewHtml(fieldConfig, null);

        assertEquals("html", result);
    }

    @Test
    public void tesGetObjectKey_validObjectKey() throws Exception {
        LinkCustomFieldConfig config = new LinkCustomFieldConfig(null, null, null);
        String result = config.getObjectKey();
        assertEquals("LinkCustomFieldConfig", result);
    }

    @Test
    public void getConfigurationObject() throws Exception {
        CustomField cf = mock(CustomField.class);
        CustomFieldType cft = mock(CustomFieldType.class);
        FieldConfig fieldConfig = mock(FieldConfig.class);

        when(fieldConfig.getCustomField()).thenReturn(cf);
        when(cf.getCustomFieldType()).thenReturn(cft);
        when(cft.getDefaultValue(fieldConfig)).thenReturn("default");

        LinkCustomFieldConfig config = new LinkCustomFieldConfig(null, null, null);

        Object result = config.getConfigurationObject(null, fieldConfig);
        assertEquals("default", result.toString());

    }

    @Test
    public void testGetBaseEditUrl_validUrl() throws Exception {
        LinkCustomFieldConfig config = new LinkCustomFieldConfig(null, null, null);
        String result = config.getBaseEditUrl();
        assertEquals("LinkCustomFieldConfigAction!default.jspa", result);
    }

}