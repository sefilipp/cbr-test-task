package ru.cbr.atlas.jira.testtask.action;

import com.atlassian.jira.issue.fields.CustomField;
import org.junit.Test;
import ru.cbr.atlas.jira.testtask.manager.LinkCustomFieldConfigManager;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;


public class LinkCustomFieldConfigActionTest {
    @Test
    public void testDoDefault_inputString() throws Exception {
        LinkCustomFieldConfigManager manager = mock(LinkCustomFieldConfigManager.class);

        LinkCustomFieldConfigAction action = spy(new LinkCustomFieldConfigAction(null, manager));
        doReturn(1l).when(action).getFieldConfigId();

        String result = action.doDefault();

        assertEquals("input", result);
    }

    @Test
    public void testDoExecute_redirectString() throws Exception {
        CustomField cf = mock(CustomField.class);
        when(cf.getIdAsLong()).thenReturn(1l);
        LinkCustomFieldConfigManager manager = mock(LinkCustomFieldConfigManager.class);

        LinkCustomFieldConfigAction action = spy(new LinkCustomFieldConfigAction(null, manager));
        doReturn(1l).when(action).getFieldConfigId();
        doReturn(cf).when(action).getCustomField();
        doReturn("default").when(action).getRedirect("ConfigureCustomField!default.jspa?fieldConfigId=1&customFieldId=1");

        String result = action.doExecute();

        assertEquals("default", result);
    }

    @Test
    public void testGetterAndSetter() throws Exception {

        LinkCustomFieldConfigAction action = new LinkCustomFieldConfigAction(null, null);

        assertNull(action.getMode());
        action.setMode(true);
        assertTrue(action.getMode());
    }

}