package ru.cbr.atlas.jira.testtask.field;

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.MockCustomField;
import com.atlassian.jira.issue.fields.config.FieldConfig;
import com.atlassian.jira.issue.fields.config.FieldConfigItemType;
import com.atlassian.jira.issue.fields.layout.field.FieldLayoutItem;
import com.atlassian.jira.issue.fields.layout.field.MockFieldLayoutItem;
import com.atlassian.jira.mock.issue.MockIssue;
import org.junit.Test;
import ru.cbr.atlas.jira.testtask.manager.LinkCustomFieldConfigManager;

import java.util.List;
import java.util.Map;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class LinkCustomFieldTest {
    @Test
    public void testGetVelocityParameters_validParams() throws Exception {
        Issue issue = new MockIssue();
        CustomField customField = mock(CustomField.class);
        FieldLayoutItem layoutItem = new MockFieldLayoutItem();
        FieldConfig fieldConfig = mock(FieldConfig.class);
        LinkCustomFieldConfigManager linkCustomFieldConfigManager = mock(LinkCustomFieldConfigManager.class);

        when(customField.getRelevantConfig(issue)).thenReturn(fieldConfig);
        when(fieldConfig.getId()).thenReturn(1l);
        when(linkCustomFieldConfigManager.getMode("1")).thenReturn("true");

        LinkCustomField cf = new LinkCustomField(null, null, null, null, null, null, linkCustomFieldConfigManager);

        Map<String, Object> resultParameters = cf.getVelocityParameters(issue, customField, layoutItem);

        assertEquals(2, resultParameters.size());
        assertTrue((Boolean) resultParameters.get("mode"));
    }

    @Test
    public void testIsRenderable_false() throws Exception {
        LinkCustomField cf = new LinkCustomField(null, null, null, null, null, null, null);
        boolean renderable = cf.isRenderable();
        assertFalse(renderable);
    }

    @Test
    public void testGetConfigurationItemTypes_linkCustomFieldConfigInResult() throws Exception {
        LinkCustomField cf = new LinkCustomField(null, null, null, null, null, null, null);

        List<FieldConfigItemType> result = cf.getConfigurationItemTypes();
        assertTrue(result.get(1) instanceof LinkCustomFieldConfig);
    }
}