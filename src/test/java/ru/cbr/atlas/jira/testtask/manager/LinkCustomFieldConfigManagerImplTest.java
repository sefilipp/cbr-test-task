package ru.cbr.atlas.jira.testtask.manager;

import com.atlassian.sal.api.pluginsettings.PluginSettings;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.*;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.*;

public class LinkCustomFieldConfigManagerImplTest {

    private static PluginSettingsFactory pluginSettingsFactory;
    private static LinkCustomFieldConfigManagerImpl manager;
    private static PluginSettings pluginSettings;

    @BeforeClass
    public static void setup() {
        pluginSettings = mock(PluginSettings.class);
        pluginSettingsFactory = mock(PluginSettingsFactory.class);
        when(pluginSettingsFactory.createSettingsForKey("CBR_LINK_CUSTOM_FIELD")).thenReturn(pluginSettings);
        manager = new LinkCustomFieldConfigManagerImpl(pluginSettingsFactory);
    }

    @Test
    public void testSetMode() throws Exception {
        manager.setMode("key", "mode");
        verify(pluginSettings, times(1)).put(eq("key"), eq("mode"));
    }

    @Test
    public void testGetMode() throws Exception {
        when(pluginSettings.get("key")).thenReturn("mode");
        String result = manager.getMode("key");
        assertEquals("mode", result);
    }

}