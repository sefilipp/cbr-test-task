package ru.cbr.atlas.jira.testtask.field;

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.fields.config.FieldConfig;
import com.atlassian.jira.issue.fields.config.FieldConfigItemType;
import com.atlassian.jira.issue.fields.layout.field.FieldLayoutItem;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.velocity.VelocityManager;
import ru.cbr.atlas.jira.testtask.manager.LinkCustomFieldConfigManager;

import java.util.HashMap;
import java.util.Map;

public class LinkCustomFieldConfig implements FieldConfigItemType {

    private final JiraAuthenticationContext jiraAuthenticationContext;
    private final VelocityManager velocityManager;
    private final LinkCustomFieldConfigManager linkCustomFieldConfigManager;

    public LinkCustomFieldConfig(JiraAuthenticationContext jiraAuthenticationContext, VelocityManager velocityManager,
                                 LinkCustomFieldConfigManager linkCustomFieldConfigManager) {
        this.jiraAuthenticationContext = jiraAuthenticationContext;
        this.velocityManager = velocityManager;
        this.linkCustomFieldConfigManager = linkCustomFieldConfigManager;
    }

    @Override
    public String getDisplayName() {
        return "ru.cbr.atlas.jira.testtask.field.config.name";
    }

    @Override
    public String getDisplayNameKey() {
        return "ru.cbr.atlas.jira.testtask.field.config.name";
    }

    @Override
    public String getViewHtml(FieldConfig fieldConfig, FieldLayoutItem fieldLayoutItem) {
        Map<String, Object> context = new HashMap<String, Object>();
        context.put("i18n", jiraAuthenticationContext.getI18nHelper());
        context.put("mode", linkCustomFieldConfigManager.getMode(String.valueOf(fieldConfig.getId())));
        return velocityManager.getBody("/ru/cbr/atlas/jira/testtask/templates/", "view-link-config.vm", context);
    }

    @Override
    public String getObjectKey() {
        return "LinkCustomFieldConfig";
    }

    @Override
    public Object getConfigurationObject(Issue issue, FieldConfig fieldConfig) {
        return fieldConfig.getCustomField().getCustomFieldType().getDefaultValue(fieldConfig);
    }

    @Override
    public String getBaseEditUrl() {
        return "LinkCustomFieldConfigAction!default.jspa";
    }
}
