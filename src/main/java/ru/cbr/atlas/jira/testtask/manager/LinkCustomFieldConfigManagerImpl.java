package ru.cbr.atlas.jira.testtask.manager;


import com.atlassian.sal.api.pluginsettings.PluginSettings;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;

public class LinkCustomFieldConfigManagerImpl implements LinkCustomFieldConfigManager{
    public static final String PLUGIN_KEY = "CBR_LINK_CUSTOM_FIELD";

    private final PluginSettings pluginSettings;

    public LinkCustomFieldConfigManagerImpl(PluginSettingsFactory pluginSettingsFactory) {
        this.pluginSettings = pluginSettingsFactory.createSettingsForKey(PLUGIN_KEY);
    }

    @Override
    public void setMode(String key, String mode){
        pluginSettings.put(key, mode);
    }

    @Override
    public String getMode(String key){
        Object mode = pluginSettings.get(key);
        if (mode != null){
            return (String) mode;
        }
        return null;
    }
}
