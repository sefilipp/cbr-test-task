package ru.cbr.atlas.jira.testtask.manager;

public interface LinkCustomFieldConfigManager {

    String getMode(String key);
    void setMode(String key, String mode);
}
