package ru.cbr.atlas.jira.testtask.action;

import com.atlassian.jira.config.managedconfiguration.ManagedConfigurationItemService;
import com.atlassian.jira.web.action.admin.customfields.AbstractEditConfigurationItemAction;
import ru.cbr.atlas.jira.testtask.manager.LinkCustomFieldConfigManager;

public class LinkCustomFieldConfigAction extends AbstractEditConfigurationItemAction {

    private final LinkCustomFieldConfigManager linkCustomFieldConfigManager;

    private Boolean mode;

    protected LinkCustomFieldConfigAction(ManagedConfigurationItemService managedConfigurationItemService,
                                          LinkCustomFieldConfigManager linkCustomFieldConfigManager) {
        super(managedConfigurationItemService);
        this.linkCustomFieldConfigManager = linkCustomFieldConfigManager;
    }

    @Override
    public String doDefault() throws Exception {
        Boolean data = Boolean.valueOf(linkCustomFieldConfigManager.getMode(getFieldConfigId().toString()));
        if (data != null) {
            mode = data;
        }
        return INPUT;
    }

    @Override
    protected String doExecute() throws Exception {
        linkCustomFieldConfigManager.setMode(getFieldConfigId().toString(), String.valueOf(Boolean.TRUE.equals(mode)));
        return getRedirect("ConfigureCustomField!default.jspa?" + getUrlParametersPart());
    }

    private String getUrlParametersPart() {
        return "fieldConfigId=" + getFieldConfigId() + "&customFieldId=" + getCustomField().getIdAsLong();
    }

    public Boolean getMode() {
        return mode;
    }

    public void setMode(Boolean mode) {
        this.mode = mode;
    }
}
