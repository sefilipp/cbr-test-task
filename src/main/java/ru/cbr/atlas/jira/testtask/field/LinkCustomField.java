package ru.cbr.atlas.jira.testtask.field;

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.customfields.impl.TextAreaCFType;
import com.atlassian.jira.issue.customfields.manager.GenericConfigManager;
import com.atlassian.jira.issue.customfields.persistence.CustomFieldValuePersister;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.TextFieldCharacterLengthValidator;
import com.atlassian.jira.issue.fields.config.FieldConfig;
import com.atlassian.jira.issue.fields.config.FieldConfigItemType;
import com.atlassian.jira.issue.fields.layout.field.FieldLayoutItem;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.velocity.VelocityManager;
import ru.cbr.atlas.jira.testtask.manager.LinkCustomFieldConfigManager;

import javax.annotation.Nonnull;
import java.util.List;
import java.util.Map;

public class LinkCustomField extends TextAreaCFType {

    private final JiraAuthenticationContext jiraAuthenticationContext;
    private final VelocityManager velocityManager;
    private final LinkCustomFieldConfigManager linkCustomFieldConfigManager;


    public LinkCustomField(CustomFieldValuePersister customFieldValuePersister,
                           GenericConfigManager genericConfigManager,
                           TextFieldCharacterLengthValidator textFieldCharacterLengthValidator,
                           JiraAuthenticationContext jiraAuthenticationContext, JiraAuthenticationContext jiraAuthenticationContext1, VelocityManager velocityManager, LinkCustomFieldConfigManager linkCustomFieldConfigManager) {
        super(customFieldValuePersister, genericConfigManager, textFieldCharacterLengthValidator, jiraAuthenticationContext);
        this.jiraAuthenticationContext = jiraAuthenticationContext1;
        this.velocityManager = velocityManager;
        this.linkCustomFieldConfigManager = linkCustomFieldConfigManager;
    }

    @Nonnull
    @Override
    public Map<String, Object> getVelocityParameters(Issue issue, CustomField field, FieldLayoutItem fieldLayoutItem) {
        Map<String, Object> parameters = super.getVelocityParameters(issue, field, fieldLayoutItem);
        if (issue == null) {
            return parameters;
        }

        FieldConfig fieldConfig = field.getRelevantConfig(issue);
        Boolean mode = Boolean.valueOf(linkCustomFieldConfigManager.getMode(String.valueOf(fieldConfig.getId())));
        parameters.put("mode", mode);

        return parameters;
    }

    @Override
    public boolean isRenderable() {
        return false;
    }

    @Nonnull
    @Override
    public List<FieldConfigItemType> getConfigurationItemTypes() {
        final List<FieldConfigItemType> configurationItemTypes = super.getConfigurationItemTypes();
        configurationItemTypes.add(new LinkCustomFieldConfig(jiraAuthenticationContext, velocityManager, linkCustomFieldConfigManager));
        return configurationItemTypes;
    }
}
